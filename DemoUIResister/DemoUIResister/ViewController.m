//
//  ViewController.m
//  DemoUIResister
//
//  Created by click labs 127 on 9/17/15.
//  Copyright (c) 2015 vijay kumar. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()<UINavigationBarDelegate,UITextFieldDelegate>
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createNotificationBar];
  }


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


-(void)createNotificationBar
{
    NSMutableDictionary * dict = [[NSMutableDictionary alloc]init];
    [dict setValue:@"EDIT PROFILE" forKey:@"name"];
    
    UIImageView *backImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [backImage setImage:[UIImage imageNamed:@"bg_iphone5.png"]];
    backImage.userInteractionEnabled = true;
    [self.view addSubview:backImage];
    
    UIView *lineViewObject = [[UIView alloc]initWithFrame:CGRectMake(150, 258, 300, 1)];
    lineViewObject.backgroundColor=[UIColor colorWithRed:18/255.0f green:63/255.0f blue:128/255.0f alpha:1.0f];
    [backImage addSubview:lineViewObject];
    
    UIImageView *roundImage = [[UIImageView alloc]initWithFrame:CGRectMake(80, 70, 145,150)];
    [roundImage setImage:[UIImage imageNamed:@"Example2.png"]];
    roundImage.userInteractionEnabled = true;
    [backImage addSubview:roundImage];
    
    UILabel *infoLabelObject = [[UILabel alloc]initWithFrame:CGRectMake(10, 210, 200, 100)];
    [infoLabelObject setText:@"PERSONAL INFO"];
    infoLabelObject.textColor=[UIColor colorWithRed:18/255.0f green:63/255.0f blue:128/255.0f alpha:1.0f];
    [infoLabelObject setFont:[UIFont fontWithName:@"Avenir-Book" size:17.0f]];
    [backImage addSubview:infoLabelObject];

//    UIScrollView *scrollViewObject = [[UIScrollView alloc]initWithFrame:CGRectMake(0,0,500,800)];
//    scrollViewObject.contentSize =CGSizeMake(0,900);
//    scrollViewObject.backgroundColor = [UIColor blackColor];
//    scrollViewObject.delegate =self;
//    //scrollViewObject.alpha = 0.5;
//    scrollViewObject.scrollEnabled =YES;
//    [self.view addSubview:scrollViewObject];
    
    UINavigationBar * notificationObject = [[UINavigationBar alloc]initWithFrame:CGRectMake(0, 0, 350, 60)];
    self.navigationController.navigationBar.tintColor=[UIColor colorWithRed:18/255.0f green:63/255.0f blue:128/255.0f alpha:1.0f];
    [notificationObject setTitleTextAttributes:dict];
    notificationObject.userInteractionEnabled = true;
    [self.view addSubview:notificationObject];
    
    UIView *upperViewObject = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 350, 60)];
    upperViewObject.backgroundColor=[UIColor colorWithRed:32/255.0f green:73/255.0f blue:127/255.0f alpha:1.0f];
    [notificationObject addSubview:upperViewObject];
    
    UIButton *buttonObject = [[UIButton alloc]initWithFrame:CGRectMake(10, 25, 30, 30)];
    UIImage *imageObject = [UIImage imageNamed:@"dc0fa3f38f18c9d1b4e2c99967401405-Arrow.png"];
    [buttonObject setImage:imageObject forState:UIControlStateNormal];
    [notificationObject addSubview:buttonObject];
    
    UIButton *changePassObject = [[UIButton alloc]initWithFrame:CGRectMake(60, 502, 200, 100)];
    [changePassObject setTitle:@"CHANGE PASSWORD" forState:UIControlStateNormal];
    changePassObject.titleLabel.font = [UIFont systemFontOfSize:11.0F];
    [changePassObject setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [backImage addSubview:changePassObject];
    
    UILabel *labelObject = [[UILabel alloc]initWithFrame:CGRectMake(95, -9, 150, 100)];
    [labelObject setText:@"EDIT PROFILE"];
    [labelObject setFont:[UIFont fontWithName:@"ArialRoundedMTBold"  size:20.0f]];
    [labelObject setTextColor:[UIColor whiteColor]];
    [notificationObject addSubview:labelObject];
    
    UITextField *firstFieldObject = [[UITextField alloc]initWithFrame:CGRectMake(10, 285, 300, 44)];
    UIColor *firstFieldColor = [UIColor grayColor];
    
    
    firstFieldObject.attributedPlaceholder=[[NSAttributedString alloc] initWithString:@" STEVE" attributes:@{NSForegroundColorAttributeName: firstFieldColor}];
    firstFieldObject.borderStyle = UITextBorderStyleLine;
    firstFieldObject.layer.borderColor=[[UIColor colorWithRed:32/255.0f green:73/255.0f blue:127/255.0f alpha:1.0f]CGColor];
    firstFieldObject.userInteractionEnabled = YES;
    firstFieldObject.textAlignment = UITextLayoutDirectionLeft;
    [backImage addSubview:firstFieldObject];

    UITextField *secondFieldObject = [[UITextField alloc]initWithFrame:CGRectMake(10, 335, 300, 44)];
    UIColor *secondFieldColor = [UIColor grayColor];
    secondFieldObject.attributedPlaceholder=[[NSAttributedString alloc] initWithString:@" STEPHEN" attributes:@{NSForegroundColorAttributeName: secondFieldColor}];
    secondFieldObject.borderStyle = UITextBorderStyleLine;
    secondFieldObject.textAlignment = UITextLayoutDirectionLeft;
    [backImage addSubview:secondFieldObject];

    UITextField *emailFieldObject = [[UITextField alloc]initWithFrame:CGRectMake(10, 385, 300, 44)];
    UIColor *emailFieldColor = [UIColor colorWithRed:32/255.0f green:73/255.0f blue:127/255.0f alpha:1.0f];
    emailFieldObject.attributedPlaceholder=[[NSAttributedString alloc] initWithString:@" STEVE.STEPHEN@YAHOOMAIL.COM" attributes:@{NSForegroundColorAttributeName: emailFieldColor}];
    emailFieldObject.borderStyle = UITextBorderStyleLine;
    emailFieldObject.keyboardType= UIKeyboardTypeEmailAddress;
    emailFieldObject.textAlignment = UITextLayoutDirectionLeft;
    [backImage addSubview:emailFieldObject];

    UITextField *numberFieldObject = [[UITextField alloc]initWithFrame:CGRectMake(10, 435, 300, 44)];
    UIColor *numberFieldColor = [UIColor colorWithRed:27/255.0f green:65/255.0f blue:127/255.0f alpha:1.0f];
    numberFieldObject.attributedPlaceholder=[[NSAttributedString alloc] initWithString:@" +1-444-242-6565" attributes:@{NSForegroundColorAttributeName: numberFieldColor}];
    numberFieldObject.borderStyle = UITextBorderStyleLine;
    numberFieldObject.keyboardType=UIKeyboardTypeNumberPad;
    numberFieldObject.textAlignment = UITextLayoutDirectionLeft;
    [backImage addSubview:numberFieldObject];
    
    UIButton *registerObject = [[UIButton alloc]initWithFrame:CGRectMake(10, 495, 300, 44)];
    //[registerObject setBackgroundColor:[UIColor colorWithRed:18/255.0f green:63/255.0f blue:128/255.0f alpha:1.0f]];//
    registerObject.backgroundColor=[UIColor colorWithRed:32/255.0f green:73/255.0f blue:127/255.0f alpha:1.0f];
    [registerObject setTitle:@"Done" forState:UIControlStateNormal];
    [backImage addSubview:registerObject];
    
    
    
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}




@end
